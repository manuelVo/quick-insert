// Savage Worlds Adventure Edition integration
import { CharacterSheetContext } from "../module/contexts";
import { QuickInsert } from "../module/core";
import { getSetting, setSetting, settings } from "../module/settings";

export const SYSTEM_NAME = "swade";

export const defaultSheetFilters = {
  skill: "swade.skills",
  hindrance: "swade.hindrances",
  edge: "swade.edges",
  power: "",
  weapon: "",
  armor: "",
  shield: "",
  gear: "",
  "character.choice": "",
  "vehicle.choice": "",
  mod: "",
  "vehicle-weapon": "",
};

export class SwadeSheetContext extends CharacterSheetContext {
  constructor(
    documentSheet: DocumentSheet,
    anchor: JQuery<HTMLElement>,
    sheetType?: string,
    insertType?: string
  ) {
    super(documentSheet, anchor);
    if (sheetType && insertType) {
      const sheetFilters = getSetting(settings.FILTERS_SHEETS).baseFilters;
      this.filter =
        sheetFilters[`${sheetType}.${insertType}`] || sheetFilters[insertType];
    }
  }
}

export function sheetSwadeRenderHook(
  app: DocumentSheet,
  sheetType?: string
): void {
  if (app.element.find(".quick-insert-link").length > 0) {
    return;
  }
  const link = `<a class="quick-insert-link" title="Quick Insert"><i class="fas fa-search"></i></a>`;
  app.element.find("a.item-create").each((i, el) => {
    const type = el.dataset.type || "";
    if (!Object.keys(defaultSheetFilters).includes(type)) return;
    const linkEl = $(link);
    $(el).after(linkEl);
    linkEl.on("click", () => {
      const context = new SwadeSheetContext(app, linkEl, sheetType, type);
      QuickInsert.open(context);
    });
  });
}

export function init(): void {
  if (game.user?.isGM) {
    const customFilters = getSetting(settings.FILTERS_SHEETS).baseFilters;
    setSetting(settings.FILTERS_SHEETS, {
      baseFilters: {
        ...defaultSheetFilters,
        ...customFilters,
      },
    });
  }
  Hooks.on(
    "renderSwadeCharacterSheet",
    (app: DocumentSheet) =>
      getSetting(settings.FILTERS_SHEETS_ENABLED) &&
      sheetSwadeRenderHook(app, "character")
  );
  Hooks.on("renderSwadeNPCSheet", (app: DocumentSheet) =>
    sheetSwadeRenderHook(app, "npc")
  );
  Hooks.on("renderSwadeVehicleSheet", (app: DocumentSheet) =>
    sheetSwadeRenderHook(app, "vehicle")
  );

  console.log("Quick Insert | swade system extensions initiated");
}
