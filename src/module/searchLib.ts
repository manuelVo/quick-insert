import Fuse from "fuse.js";
import { settings, getSetting } from "./settings";
import { TimeoutError, withDeadline } from "./utils";

export enum DocumentType {
  ACTOR = "Actor",
  ITEM = "Item",
  JOURNALENTRY = "JournalEntry",
  MACRO = "Macro",
  ROLLTABLE = "RollTable",
  SCENE = "Scene",
}

export const IndexedDocumentTypes = [
  DocumentType.ACTOR,
  DocumentType.ITEM,
  DocumentType.JOURNALENTRY,
  DocumentType.MACRO,
  DocumentType.ROLLTABLE,
  DocumentType.SCENE,
];

export const DocumentMeta = {
  [DocumentType.ACTOR]: CONFIG.Actor.documentClass.metadata,
  [DocumentType.ITEM]: CONFIG.Item.documentClass.metadata,
  [DocumentType.JOURNALENTRY]: CONFIG.JournalEntry.documentClass.metadata,
  [DocumentType.MACRO]: CONFIG.Macro.documentClass.metadata,
  [DocumentType.ROLLTABLE]: CONFIG.RollTable.documentClass.metadata,
  [DocumentType.SCENE]: CONFIG.Scene.documentClass.metadata,
};

export const documentIcons = {
  [DocumentType.ACTOR]: "fa-user",
  [DocumentType.ITEM]: "fa-suitcase",
  [DocumentType.JOURNALENTRY]: "fa-book-open",
  [DocumentType.MACRO]: "fa-terminal",
  [DocumentType.ROLLTABLE]: "fa-th-list",
  [DocumentType.SCENE]: "fa-map",
};

export function getCollectionFromType(type: DocumentType): GenericCollection {
  //@ts-ignore
  return CONFIG[type].collection.instance;
}

export interface DocumentAction {
  id: string;
  icon: string;
  title: string;
  hint?: string;
}

const ignoredFolderNames: Record<string, boolean> = { _fql_quests: true };

export function enabledDocumentTypes(): DocumentType[] {
  const disabled = getSetting(settings.INDEXING_DISABLED);
  return IndexedDocumentTypes.filter(
    (t) => !disabled?.entities?.[t]?.includes(game.user?.role)
  );
}

export function packEnabled(pack: Compendium): boolean {
  const disabled = getSetting(settings.INDEXING_DISABLED);
  // Pack entity type enabled?
  if (disabled?.entities?.[pack.metadata.entity]?.includes(game.user?.role)) {
    return false;
  }

  // Pack enabled?
  if (disabled?.packs?.[pack.collection]?.includes(game.user?.role)) {
    return false;
  }

  // Pack entity type indexed?
  if (!IndexedDocumentTypes.includes(pack.metadata.entity as DocumentType)) {
    return false;
  }

  // Not hidden?
  return !(pack.private && !game.user?.isGM);
}

export type SearchResultPredicate = (item: SearchResult) => boolean;

interface SearchItemData {
  id: string;
  uuid: string;
  name: string;
  documentType: DocumentType;
  img?: string | null;
}

export abstract class SearchItem {
  id: string;
  uuid: string;
  name: string;
  documentType: DocumentType;
  img?: string | null;

  constructor(data: SearchItemData) {
    this.id = data.id;
    this.uuid = data.uuid;
    this.name = data.name;
    this.documentType = data.documentType;
    this.img = data.img;
  }

  // Get the draggable attributes in order to make custom elements
  get draggableAttrs(): Record<string, string> {
    return {};
  }
  // Get the html for an icon that represents the item
  get icon(): string {
    return "";
  }
  // Get a clickable (preferrably draggable) link to the entity
  get link(): string {
    return "";
  }
  // Reference the entity in a journal, chat or other places that support it
  get journalLink(): string {
    return "";
  }
  // Reference the entity in a script
  get script(): string {
    return "";
  }
  // Short tagline that explains where/what this is
  get tagline(): string {
    return "";
  }
  // Show the sheet or equivalent of this search result
  async show(): Promise<void> {
    return;
  }

  // Fetch the original object (or null if no longer available).
  // NEVER call as part of indexing or filtering.
  // It can be slow and most calls will cause a request to the database!
  // Call it once a decision is made, do not call for every SearchItem!
  async get(): Promise<any> {
    return null;
  }
}

export class EntitySearchItem extends SearchItem {
  folder?: { id: string; name: string };

  static fromEntities(entities: AnyDocument[]): EntitySearchItem[] {
    return entities
      .filter((e) => {
        return (
          e.visible && !(e.folder?.name && ignoredFolderNames[e.folder.name])
        );
      })
      .map(this.fromDocument);
  }

  static fromDocument(doc: AnyDocument): EntitySearchItem {
    if ("PDFoundry" in ui && "pdfoundry" in doc.data.flags) {
      return new PDFoundySearchItem({
        id: doc.id as string,
        uuid: doc.uuid,
        name: doc.name as string,
        documentType: doc.documentName as DocumentType,
        img: "img" in doc ? doc.img : null,
      });
    }
    return new EntitySearchItem({
      id: doc.id as string,
      uuid: doc.uuid,
      name: doc.name as string,
      documentType: doc.documentName as DocumentType,
      img: "img" in doc ? doc.img : null,
    });
  }

  // Get the draggable attributes in order to make custom elements
  get draggableAttrs(): Record<string, string> {
    return {
      draggable: "true",
      "data-entity": this.documentType,
      "data-id": this.id,
    };
  }

  // Get the draggable attributes in order to make custom elements
  get draggableAttrString(): string {
    return `draggable="true" data-entity="${this.documentType}" data-id="${this.id}"`;
  }

  get icon(): string {
    return `<i class="fas ${
      documentIcons[this.documentType]
    } entity-icon"></i>`;
  }

  // Get a draggable and clickable link to the entity
  get link(): string {
    return `<a class="entity-link" ${this.draggableAttrs}>${this.icon} ${this.name}</a>`;
  }

  // Reference the entity in a journal, chat or other places that support it
  get journalLink(): string {
    return `@${this.documentType}[${this.id}]{${this.name}}`;
  }

  // Reference the entity in a script
  get script(): string {
    return `game.${DocumentMeta[this.documentType].collection}.get("${
      this.id
    }")`;
  }

  // Short tagline that explains where/what this is
  get tagline(): string {
    if (this.folder) {
      return `${this.folder.name} - ${this.documentType}`;
    }
    return `${this.documentType}`;
  }

  async show(): Promise<void> {
    (await this.get())?.sheet?.render(true);
  }

  async get(): Promise<AnyDocument | undefined> {
    return getCollectionFromType(this.documentType).get(this.id);
  }

  constructor(data: SearchItemData & { folder?: Folder }) {
    super(data);

    const folder = data.folder;
    if (folder) {
      this.folder = {
        id: folder.id as string,
        name: folder.name as string,
      };
    }
  }
}

export class PDFoundySearchItem extends EntitySearchItem {
  get icon(): string {
    return `<img class="pdf-thumbnail" src="modules/pdfoundry/assets/pdf_icon.svg" alt="PDF Icon">`;
  }
  get journalLink(): string {
    return `@PDF[${this.name}|page=1]{${this.name}}`;
  }
  async show(): Promise<void> {
    const entity = await this.get();
    (ui as any)?.PDFoundry.openPDFByName(this.name, { entity });
  }
}

export class CompendiumSearchItem extends SearchItem {
  package: string;
  packageName: string;

  static fromCompendium(compendium: Compendium): CompendiumSearchItem[] {
    const cIndex = compendium.index;
    return cIndex.map(
      (item: Compendium.IndexEntry) =>
        new CompendiumSearchItem(compendium, item)
    );
  }

  constructor(pack: Compendium, item: Compendium.IndexEntry) {
    const packName = pack.collection;
    super({
      id: item._id,
      uuid: `Compendium.${packName}.${item._id}`,
      name: item.name as string,
      documentType: pack.metadata.entity as DocumentType,
      img: item.img,
    });
    this.package = packName;
    this.packageName = pack?.metadata?.label || pack.title;
    this.documentType = pack.metadata.entity as DocumentType;
    this.uuid = `Compendium.${this.package}.${this.id}`;
  }

  // Get the draggable attributes in order to make custom elements
  get draggableAttrs(): Record<string, string> {
    return {
      draggable: "true",
      "data-pack": this.package,
      "data-id": this.id,
      "data-lookup": this.id,
    };
  }

  // Get the draggable attributes in order to make custom elements
  get draggableAttrsString(): string {
    // data-id is kept for pre-5.5 compat
    return `draggable="true" data-pack="${this.package}" data-id="${this.id}" data-lookup="${this.id}"`;
  }

  get icon(): string {
    return `<i class="fas ${
      documentIcons[this.documentType]
    } entity-icon"></i>`;
  }

  // Get a draggable and clickable link to the entity
  get link(): string {
    return `<a class="entity-link" ${this.draggableAttrs}>${this.icon} ${this.name}</a>`;
  }

  // Reference the entity in a journal, chat or other places that support it
  get journalLink(): string {
    return `@Compendium[${this.package}.${this.id}]{${this.name}}`;
  }

  // Reference the entity in a script
  get script(): string {
    return `fromUuid("${this.uuid}")`; // TODO: note that this is async somehow?
  }

  // Short tagline that explains where/what this is
  get tagline(): string {
    return `${this.packageName}`;
  }

  async show(): Promise<void> {
    (await this.get())?.sheet?.render(true);
  }

  async get(): Promise<AnyDocument | null> {
    return (await fromUuid(this.uuid)) as AnyDocument;
  }
}

export function isEntity(item: SearchItem): item is EntitySearchItem {
  return item instanceof EntitySearchItem;
}
export function isCompendiumEntity(
  item: SearchItem
): item is CompendiumSearchItem {
  return item instanceof CompendiumSearchItem;
}

export interface SearchResult {
  item: SearchItem;
  match: readonly Fuse.FuseResultMatch[];
}

class FuseSearchIndex {
  fuse: Fuse<SearchItem>;

  constructor() {
    this.fuse = new Fuse([], {
      keys: ["name"],
      includeMatches: true,
      threshold: 0.3,
    });
  }

  addAll(items: SearchItem[]) {
    for (const item of items) {
      this.fuse.add(item);
    }
  }

  add(item: SearchItem): void {
    this.fuse.add(item);
  }

  removeByUuid(uuid: string): void {
    this.fuse.remove((i) => i?.uuid == uuid);
  }

  search(query: string): SearchResult[] {
    return this.fuse.search(query).map((res) => ({
      item: res.item,
      match: res.matches as readonly Fuse.FuseResultMatch[],
    }));
  }
}

export class SearchLib {
  index: FuseSearchIndex;

  constructor() {
    this.index = new FuseSearchIndex();
  }

  indexCompendium(compendium?: Compendium): void {
    if (!compendium) return;
    if (packEnabled(compendium)) {
      const index = CompendiumSearchItem.fromCompendium(compendium);
      this.index.addAll(index);
    }
  }

  async indexCompendiums(refresh = false): Promise<void> {
    if (!game.packs) return;

    for await (const res of loadIndexes(refresh)) {
      if (res.error) {
        console.log("Quick Insert | Index loading failure", res);
        continue;
      }
      console.log("Quick Insert | Index loading success", res);
      this.indexCompendium(game.packs.get(res.pack));
    }
  }

  indexDocuments(): void {
    for (const type of enabledDocumentTypes()) {
      this.index.addAll(
        EntitySearchItem.fromEntities(getCollectionFromType(type).contents)
      );
    }
  }

  addItem(item: SearchItem): void {
    this.index.add(item);
  }

  removeItem(entityUuid: string): void {
    this.index.removeByUuid(entityUuid);
  }

  replaceItem(item: SearchItem): void {
    this.removeItem(item.uuid);
    this.addItem(item);
  }

  search(
    text: string,
    filter: SearchResultPredicate | null,
    max: number
  ): SearchResult[] {
    if (filter) {
      return this.index.search(text).filter(filter).slice(0, max);
    }
    return this.index.search(text).slice(0, max);
  }
}

export function formatMatch(
  result: SearchResult,
  formatFn: (str: string) => string
): string {
  const match = result.match[0];
  if (!match.value) return "";
  let text = match.value;
  [...match.indices].reverse().forEach(([start, end]) => {
    // if (start === end) return;
    text =
      text.substring(0, start) +
      formatFn(text.substring(start, end + 1)) +
      text.substring(end + 1);
  });
  return text;
}

interface IndexStatus {
  error?: Error;
  pack: string;
  packsLeft: number;
  errorCount: number;
}

// refresh = request new index for all compendiums
export async function* loadIndexes(
  refresh: boolean
): AsyncGenerator<IndexStatus> {
  if (!game.packs) {
    console.error("Can't load indexes before packs are initialized");
    return;
  }

  // Information about failures
  const failures: {
    [pack: string]: {
      errors: number;
      waiting?: Promise<unknown>;
    };
  } = {};

  const timeout = getSetting(settings.INDEX_TIMEOUT);

  const packsRemaining: Compendium<Application.Options>[] = [];
  for (const pack of game.packs) {
    if (packEnabled(pack)) {
      failures[pack.collection] = { errors: 0 };
      packsRemaining.push(pack);
    }
  }

  while (packsRemaining.length > 0) {
    const pack = packsRemaining.shift();
    if (!pack) break;

    let promise: Promise<unknown> | undefined;

    // Refresh pack index
    if (refresh || pack.index.length === 0) {
      try {
        promise = failures[pack.collection].waiting ?? pack.getIndex();

        await withDeadline(
          promise,
          timeout * (failures[pack.collection].errors + 1)
        );
      } catch (error) {
        ++failures[pack.collection].errors;
        if (error instanceof TimeoutError) {
          failures[pack.collection].waiting = promise;
        } else {
          delete failures[pack.collection].waiting;
        }

        yield {
          error: error,
          pack: pack.collection,
          packsLeft: packsRemaining.length,
          errorCount: failures[pack.collection].errors,
        };
        if (failures[pack.collection].errors <= 4) {
          // Pack failed, will be retried later.
          packsRemaining.push(pack);
        } else {
          console.warn(
            `Quick Insert | Package "${pack.collection}" could not be indexed `
          );
        }
        continue;
      }
    }

    yield {
      pack: pack.collection,
      packsLeft: packsRemaining.length,
      errorCount: failures[pack.collection].errors,
    };
  }
}
