import { EmbeddedContext } from "./contexts";
import { SearchFilter, SearchFilterConfig, FilterType } from "./searchFilters";
import { QuickInsert } from "./core";
import { enabledDocumentTypes, packEnabled } from "./searchLib";
import { i18n } from "./utils";

export function parseFilterConfig(collections: string[]): SearchFilterConfig {
  const filters: SearchFilterConfig = {
    folders: [],
    compendiums: [],
    entities: [],
  };

  for (const coll of collections) {
    const x = coll.indexOf(".");
    const base = coll.slice(0, x);
    const rest = coll.slice(x + 1);

    if (base === "Folder") {
      if (rest === "Any") {
        filters.folders = "any";
      } else if (!(typeof filters.folders === "string")) {
        filters.folders.push(rest);
      }
    } else if (base === "Compendium") {
      if (rest === "Any") {
        filters.compendiums = "any";
      } else if (!(typeof filters.compendiums === "string")) {
        filters.compendiums.push(rest);
      }
    } else if (base === "Document" || base === "Entity") {
      if (rest === "Any") {
        filters.entities = "any";
      } else if (!(typeof filters.entities === "string")) {
        filters.entities.push(rest);
      }
    }
  }
  return filters;
}

export class FilterEditor extends Application {
  get element(): JQuery<HTMLElement> {
    return super.element as JQuery<HTMLElement>;
  }
  filter: SearchFilter;
  searchInput = "";

  idPrefix: RegExp;

  constructor(filter: SearchFilter) {
    super({
      title: i18n("FilterEditorTitle"),
      classes: ["filter-editor"],
      template: "modules/quick-insert/templates/filter-editor.hbs",
      resizable: true,
      width: 550,
      height: 560,
      scrollY: [
        ".collection-list.compendium-list",
        ".collection-list.directory-list",
        ".collection-list.entity-list",
      ],
    });
    this.filter = filter;
    this.idPrefix = new RegExp(`^${this.filter.id}_`);
  }

  prefix(name: string): string {
    return `${this.filter.id}_${name}`;
  }
  unPrefix(name: string): string {
    return name.replace(this.idPrefix, "");
  }

  render(force?: boolean, options?: Application.RenderOptions): unknown {
    return super.render(force, options);
  }

  isEditable(): boolean {
    return Boolean(
      this.filter.type == FilterType.Client ||
        (this.filter.type == FilterType.World && game.user?.isGM)
    );
  }

  fixAny(
    type: string,
    form: JQuery<HTMLFormElement>,
    formData: JQuery.NameValuePair[]
  ): void {
    form
      .find(`input[name^="${this.filter.id}_${type}."].disabled`)
      .removeClass("disabled");

    const selectedAny = formData.find((r) => r.name.endsWith(".Any"));
    if (selectedAny) {
      const other = form.find(
        `input[name^="${this.filter.id}_${type}."]:not(input[name="${this.filter.id}_${selectedAny.name}"])`
      );
      other.prop("checked", false);
      other.addClass("disabled");
    }
  }

  close(): Promise<void> {
    if (this.element.find(".quick-insert").length > 0 && QuickInsert.app) {
      QuickInsert.app.embeddedMode = false;
      QuickInsert.app.closeDialog();
    }

    return super.close();
  }

  processForm(): {
    name: string;
    title: string;
    formData: JQuery.NameValuePair[];
  } {
    const form = this.element.find("form");

    let formData = form.serializeArray();
    formData.forEach((d) => {
      d.name = this.unPrefix(d.name);
    });

    const name = formData.find((p) => p.name == "name")?.value.trim() as string;
    const title = formData.find((p) => p.name == "title")?.value as string;
    formData = formData.filter((p) => p.name != "name" && p.name != "title");
    const compendiums = formData.filter((r) =>
      r.name.startsWith("Compendium.")
    );
    const folders = formData.filter((r) => r.name.startsWith("Folder."));
    const entity = formData.filter((r) => r.name.startsWith("Document."));

    this.fixAny("Compendium", form, compendiums);
    this.fixAny("Folder", form, folders);
    this.fixAny("Document", form, entity);

    return {
      name,
      title,
      formData,
    };
  }

  formChange(): void {
    if (!this.isEditable()) return;

    const { name, title, formData } = this.processForm();

    const config = parseFilterConfig(formData.map((x) => x.name));
    const oldTag = this.filter.tag;

    if (name != "") {
      this.filter.tag = name;
    }

    this.filter.subTitle = title;
    this.filter.filterConfig = config;

    // Hacky way to keep/update state of input
    this.searchInput =
      QuickInsert.app?.input?.text().replace(`@${oldTag}`, "").trim() || "";

    QuickInsert.filters.filtersChanged(this.filter.type);
  }

  attachQuickInsert(): void {
    const context = new EmbeddedContext();
    context.filter = this.filter;
    context.startText = this.searchInput;

    if (!QuickInsert.app) return;

    if (QuickInsert.app.embeddedMode) {
      this.element.find(".example-out").append(QuickInsert.app.element);
    } else {
      Hooks.once(
        `render${QuickInsert.app?.constructor.name}`,
        (app: Application) => {
          this.element.find(".example-out").append(app.element);
        }
      );
    }

    QuickInsert.app.embeddedMode = true;
    QuickInsert.app.render(true, { context });
  }

  activateListeners(): void {
    this.attachQuickInsert();
    const form = this.element.find("form");
    form.on("change", () => {
      this.formChange();
    });
    this.processForm();
    if (
      this.filter.type == FilterType.Default ||
      (this.filter.type == FilterType.World && !game.user?.isGM)
    ) {
      this.element.find("input").prop("disabled", true);
    }
    this.element.find(".open-here").on("click", (evt) => {
      evt.preventDefault();
      this.attachQuickInsert();
    });
  }

  getData(): Record<string, unknown> {
    let folders: { name: string; label: string; selected: boolean }[] = [];
    if (!game.packs) return {};

    if (game.user?.isGM) {
      folders =
        game.folders?.map((folder) => ({
          label: folder.name as string,
          name: this.prefix(`Folder.${folder.id}`),
          selected: (this.filter.filterConfig?.folders as string[]).includes(
            folder.id as string
          ),
        })) || [];
    }

    return {
      tag: this.filter.tag,
      subTitle: this.filter.subTitle,
      isDefault: this.filter.type === FilterType.Default,
      forbiddenWorld: this.filter.type == FilterType.World && !game.user?.isGM,
      collections: [
        {
          name: this.prefix("Compendium.Any"),
          label: i18n("FilterEditorCompendiumAny"),
          selected: this.filter.filterConfig?.compendiums === "any",
        },
        ...game.packs
          .filter((pack) => packEnabled(pack))
          .map((pack) => ({
            name: this.prefix(`Compendium.${pack.collection}`),
            label: `${pack.metadata.label} - ${pack.collection}`,
            selected: this.filter.filterConfig?.compendiums.includes(
              pack.collection
            ),
          })),
      ],
      documentTypes: [
        {
          name: this.prefix("Document.Any"),
          label: i18n("FilterEditorEntityAny"),
          selected: this.filter.filterConfig?.entities === "any",
        },
        ...enabledDocumentTypes().map((type) => ({
          name: this.prefix(`Document.${type}`),
          label: game.i18n.localize(`DOCUMENT.${type}`),
          selected: this.filter.filterConfig?.entities.includes(type),
        })),
      ],
      folders: [
        {
          name: this.prefix("Folder.Any"),
          label: i18n("FilterEditorFolderAny"),
          selected: this.filter.filterConfig?.folders === "any",
        },
        ...folders,
      ],
    };
  }
}
