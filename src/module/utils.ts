export const i18n = (
  name: string,
  replacements?: { [key: string]: string }
): string => {
  if (replacements) {
    return game.i18n.format(`QUICKINSERT.${name}`, replacements);
  }
  return game.i18n.localize(`QUICKINSERT.${name}`);
};

// Type utils
export type TextInputElement = HTMLInputElement | HTMLTextAreaElement;
export function isTextInputElement(
  element: Element
): element is TextInputElement {
  return (
    element.tagName == "TEXTAREA" ||
    (element.tagName == "INPUT" && (element as HTMLInputElement).type == "text")
  );
}

// General utils

const ALPHA = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
export function randomId(idLength = 10): string {
  const values = new Uint8Array(idLength);
  window.crypto.getRandomValues(values);
  return String.fromCharCode(
    ...values.map((x) => ALPHA.charCodeAt(x % ALPHA.length))
  );
}

// Some black magic from the internet,
// places caret at end of contenteditable
export function placeCaretAtEnd(el: HTMLElement): void {
  el.focus();
  const range = document.createRange();
  range.selectNodeContents(el);
  range.collapse(false);
  const sel = window.getSelection();
  sel?.removeAllRanges();
  sel?.addRange(range);
}

// Simple utility function for async waiting
// Nicer to await waitFor(100) than nesting setTimeout callback hell
export function resolveAfter(msec: number): Promise<void> {
  return new Promise((res) => setTimeout(res, msec));
}

export class TimeoutError extends Error {
  constructor(timeoutMsec: number) {
    super(`did not complete within ${timeoutMsec}ms`);
  }
}

export function withDeadline<T>(
  p: Promise<T>,
  timeoutMsec: number
): Promise<T> {
  return Promise.race([
    p,
    new Promise<T>((res, rej) =>
      setTimeout(() => rej(new TimeoutError(timeoutMsec)), timeoutMsec)
    ),
  ]);
}

export function permissionListEq(a: any[], b: any[]): boolean {
  return a.length === b.length && [...a].every((value) => b.includes(value));
}
