import { SearchLib, SearchResult } from "./searchLib";
import type { SearchApp } from "./searchApp";
import { SearchFilterCollection } from "./searchFilters";
import type { SearchContext } from "./contexts";
import type { SystemIntegration } from "./systemIntegration";
import { getSetting, settings } from "./settings";

// Module singleton class that contains everything
class QuickInsertCore {
  app?: SearchApp;
  searchLib?: SearchLib;
  filters = new SearchFilterCollection();
  systemIntegration?: SystemIntegration;

  public get hasIndex(): boolean {
    return Boolean(this.searchLib?.index);
  }

  matchBoundKeyEvent(event: JQuery.KeyboardEventBase | KeyboardEvent): boolean {
    if (this.app?.embeddedMode || !event) return false;
    return KeybindLib.isBoundTo(event, "quick-insert", settings.KEY_BIND);
  }

  open(context?: SearchContext): void {
    this.app?.render(true, { context });
  }

  toggle(context?: SearchContext): void {
    if (this.app?.open) {
      this.app.closeDialog();
    } else {
      this.open(context);
    }
  }

  search(text: string, filter = null, max = 100): SearchResult[] {
    return this.searchLib?.search(text, filter, max) || [];
  }

  async forceIndex(): Promise<void> {
    return loadSearchIndex(true);
  }
}

export const QuickInsert = new QuickInsertCore();

// Ensure that only one loadSearchIndex function is running at any one time.
let isLoading = false;
export async function loadSearchIndex(refresh: boolean): Promise<void> {
  if (isLoading) return;
  isLoading = true;
  console.log("Quick Insert | Preparing search index...");
  const start = performance.now();
  QuickInsert.searchLib = new SearchLib();
  QuickInsert.searchLib.indexDocuments();

  QuickInsert.filters.resetFilters();
  QuickInsert.filters.loadDefaultFilters();
  QuickInsert.filters.loadSave();

  console.log(
    `Quick Insert | Indexing compendiums with timeout set to ${getSetting(
      settings.INDEX_TIMEOUT
    )}ms`
  );
  await QuickInsert.searchLib.indexCompendiums(refresh);

  console.log(
    `Quick Insert | Search index and filters completed. Indexed ${
      // @ts-ignore
      QuickInsert.searchLib?.index?.fuse._docs.length || 0
    } items in ${performance.now() - start}ms`
  );

  isLoading = false;
  Hooks.callAll("QuickInsert:IndexCompleted", QuickInsert);
}
